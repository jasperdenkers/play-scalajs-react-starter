lazy val server = (project in file("server"))
  .enablePlugins(PlayScala, WebScalaJSBundlerPlugin)
  .dependsOn(sharedJvm)
  .settings(commonSettings)
  .settings(
    scalaJSProjects := Seq(client),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    pipelineStages := Seq(digest, gzip),
    // triggers scalaJSPipeline when using compile or continuous compilation
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    libraryDependencies ++= Seq(
      guice
    )
  )

lazy val webpackDir = Def.setting {
  (baseDirectory in ThisProject).value / "webpack"
}

lazy val client = (project in file("client"))
  .settings(commonSettings)
  .settings(
    scalaJSUseMainModuleInitializer := true,
    webpackBundlingMode := BundlingMode.LibraryAndApplication(),
    npmDependencies in Compile ++= Seq(
      "react" -> "17.0.1",
      "react-dom" -> "17.0.1",
      "@babel/runtime" -> "7.12.5",
      "@material-ui/core" -> "4.11.2",
      "@material-ui/lab" -> "4.0.0-alpha.57",
      "formik" -> "2.2.6"
    ),
    npmDevDependencies in Compile ++= Seq(
      "webpack-merge" -> "4.2.2",
      "@babel/core" -> "7.12.10",
      "@babel/preset-env" -> "7.12.11",
      "@babel/preset-react" -> "7.12.10",
      "@babel/plugin-transform-runtime" -> "7.12.10",
      "babel-loader" -> "8.2.2"
    ),
    webpackResources := webpackDir.value * "*",
    webpackConfigFile := Some(webpackDir.value / "webpack.config.js"),
  )
  .enablePlugins(ScalaJSBundlerPlugin)
  .dependsOn(sharedJs)

lazy val shared = crossProject(JSPlatform, JVMPlatform)
  .crossType(CrossType.Pure)
  .in(file("shared"))
  .settings(commonSettings)
  .jsConfigure(_.enablePlugins(ScalaJSBundlerPlugin))
lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

lazy val commonSettings = Seq(
  scalaVersion := "2.13.4",
  organization := "gbt"
)

// Load server project at startup
onLoad in Global := (onLoad in Global).value.andThen(state => "project client" :: "fastOptJS::startWebpackDevServer" :: "project server" :: state)

// Automatically reload the build when source changes are detected
Global / onChangedBuildSource := ReloadOnSourceChanges
