const ScalaJS = require("./scalajs.webpack.config");
const Path = require('path');
const Merge = require("webpack-merge");

const rootDir = Path.resolve(__dirname, '../../../..');
const jsResourcesDir = Path.resolve(rootDir, 'src/main/js');

module.exports = Merge(ScalaJS, {
    entry: {
      client: Path.resolve(jsResourcesDir, './client.js'),
    },
    resolve: {
      alias: {
        'scalajs': Path.resolve(__dirname),
        'node_modules': Path.resolve(__dirname, "./node_modules"),
        '@babel': Path.resolve(__dirname, "./node_modules/@babel")
      }
    }
});

module.exports.module.rules = (module.exports.module.rules || []).concat({
  test: /\.js$/,
  exclude: /node_modules/,
  loader: 'babel-loader'
});