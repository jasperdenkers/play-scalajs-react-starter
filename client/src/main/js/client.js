var React = require('node_modules/react');
var ReactDOM = require('node_modules/react-dom');
var { CssBaseline, Container, TextField, Button } = require('node_modules/@material-ui/core');
var { Alert } = require('node_modules/@material-ui/lab');
var { Formik } = require('node_modules/formik');

const useAuthentication = () => {
    const getToken = () => {
        return JSON.parse(localStorage.getItem('token'));
    }

    const [token, setToken] = React.useState(getToken());

    const saveToken = (token) => {
        localStorage.setItem('token', JSON.stringify(token));
        setToken(token);
    }

    const removeToken = () => {
        localStorage.removeItem('token');
        setToken(null);
    }

    return {
        setToken: saveToken,
        removeToken,
        token
    }
}

function Template(props) {
    return (
        <Container maxWidth="xs">
            <CssBaseline />
            <h1>{props.title}</h1>
            {props.children}
        </Container>
    );
}

function Login({ setToken }) {
    const [generalError, setGeneralError] = React.useState();

    const onSubmit = async (values, { setSubmitting, setFieldError }) => {
        setGeneralError(undefined);

        await fetch('/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        }).then(response => {
            if (response.ok) {
                const token = response.json();

                setToken(token);
            } else {
                response.json().then((errors) => {
                    for (var field in errors) {
                        if (field === "")
                            setGeneralError(errors[field]);
                        else
                            setFieldError(field, errors[field].join(", "));
                    }
                });
            }

            setSubmitting(false);
        })
    }

    return (
        <Template title="Log in">
            <Formik
                initialValues={{ username: "", password: "" }}
                onSubmit={onSubmit}
            >
                {props => {
                    const {
                        values,
                        errors,
                        isSubmitting,
                        handleChange,
                        handleSubmit
                    } = props;

                    return (
                        <form onSubmit={handleSubmit}>
                            {generalError &&
                                <div>
                                    <Alert severity="error">{generalError}</Alert>
                                    <br />
                                </div>
                            }
                            <TextField
                                id="username"
                                name="username"
                                label="Username/Email"
                                value={values.username}
                                onChange={handleChange}
                                error={Boolean(errors.username)}
                                helperText={errors.username}
                                variant="outlined" margin="dense" fullWidth
                            />
                            <TextField
                                id="password"
                                name="password"
                                label="Password"
                                type="password"
                                value={values.password}
                                onChange={handleChange}
                                error={Boolean(errors.password)}
                                helperText={errors.password}
                                variant="outlined" margin="dense" fullWidth
                            />
                            <br/><br/>
                            <Button
                                type="submit"
                                disabled={isSubmitting}
                                variant="contained" color="primary" size="large" fullWidth
                            >Log in</Button>
                        </form>
                    );
                }}
            </Formik>
        </Template>
    );
}

function Dashboard({ removeToken }) {
    const logout = e => {
        e.preventDefault();
        removeToken();
    }

    return (
        <Template title="Dashboard">
            <Button onClick={logout} variant="contained" color="primary" size="large">Log out</Button>
        </Template>
    )
}

function App() {
    const { token, setToken, removeToken } = useAuthentication();

    if (!token) {
        return <Login setToken={setToken} />
    }

    return <Dashboard removeToken={removeToken} />
}

ReactDOM.render(
    <App />,
    document.getElementById('app')
);