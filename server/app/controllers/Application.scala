package controllers

import play.api.data.Form
import play.api.i18n.I18nSupport
import play.api.libs.json._
import play.api.mvc._

class Application extends InjectedController with I18nSupport {

  def index = Action {
    Ok(views.html.index())
  }

  def login = Action { implicit request =>
    loginForm.bindFromRequest().fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      _ => Ok(JsString("foo"))
    )
  }

  private val loginForm: Form[LoginData] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "username" -> nonEmptyText,
        "password" -> nonEmptyText
      )(LoginData.apply)(LoginData.unapply)
        .verifying("Invalid credentials", loginData => loginData.username == "admin" && loginData.password == "admin")
    )
  }

}

case class LoginData(username: String, password: String)
