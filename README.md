# Play + Scala.js + React starter

An app starter that builds on the [Play + Scala.js + React](https://gitlab.com/jasperdenkers/scalajs-react) example.

## Prerequisites 
 
 - The [SBT](https://www.scala-sbt.org/download.html) build tool

## Getting Started

```
sbt run
```